<?php
class SummaryMail{
	//default settings
	public $mail_to = "testing@p80w.com";//,mikegordon79@gmail.com,robcaruk@gmail.com";
	//public $cc_mail = "";
	public $from_name = "Product Inventory";
	public $subject = "Error Summary - RaceFace + Easton";
	public $message = "Summary : ";
	//public $path = "product/csvfiles/";
	
	public function attachTXTFileToMail($messageContent,$path,$file_name){
	    
		$file = $path.$file_name;
		$file_size = filesize($file);
		$message = $messageContent;
		$handle = fopen($file, "r");
		$content = fread($handle, $file_size);
		fclose($handle);
		//echo'<pre>';print_r($content);die;
		$content = chunk_split(base64_encode($content));
		
		/* Set the email header */
		// Generate a boundary
		$boundary = md5(uniqid(time()));
		//$header = "From: ".$this->from_name." <".$this->from_mail.">".PHP_EOL;
		//$header = "CC: "." <".$this->cc_mail.">".PHP_EOL;
		//$header .= "Reply-To: ".$reply_to.PHP_EOL;
		$header  = "MIME-Version: 1.0".PHP_EOL;
		// Multipart wraps the Email Content and Attachment
		$header .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"".PHP_EOL;
		$header .= "This is a multi-part message in MIME format.".PHP_EOL;
		$header .= "--".$boundary.PHP_EOL;
		// Email content
		// Content-type can be text/plain or text/html
		$header .= "Content-type:text/html; charset=iso-8859-1".PHP_EOL;
		$header .= "Content-Transfer-Encoding: 7bit".PHP_EOL;
		$header .= "$message".PHP_EOL;
		$header .= "--".$boundary."--";
		//Attachment
		// Edit content type for different file extensions
		$header .= "Content-Type: text/css; name=\"".$file_name."\"".PHP_EOL;
		$header .= "Content-Transfer-Encoding: base64".PHP_EOL;
		$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"".PHP_EOL;
		$header .= $content;
		$header .= "--".$boundary."--"; 
		if($this->sendMail($this->mail_to,$this->subject,$message,$header)){
			return true;
		}else{
			return false;
		}
		
	}
	public function sendMail($mail_to = "",$subject="",$message="",$header){
	    

		$status = true;
		if (mail($mail_to,$subject, $message, $header)) {
			echo PHP_EOL."Mail has been sent succesfully ";	
		} else {
			echo "Error in ".$message." mail not send !";
			$status = false;	
		}
		//file_put_contents('test.txt', sprintf($status));	
		return $status;
	}
	public function sendSummeryMail($mail_to="",$subject="",$message="",$mailDetails){
		//$header = "From: ".$this->from_name." <".$this->from_mail.">".PHP_EOL;
		$header = "CC: "." <".$this->cc_mail.">".PHP_EOL;
		$header .= "MIME-Version: 1.0".PHP_EOL;	
		$header .= "Content-type: text/html".PHP_EOL;
		$status=$this->sendMail($this->mail_to,$mailDetails->subject, $mailDetails->body, $header);	
		return $status;	
	}
}
 
