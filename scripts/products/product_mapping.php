<?php
set_time_limit(0);
require_once dirname(__FILE__) . '/../../lib/Logging.php';
require_once dirname(__FILE__) . '/../../model/shopify_product.php';

class productImport
{
    private $logDirPath;
    private $fileToLog;
    private $csvFilePath;
    private $logFiles;
    private $storeName;
    private $debug;
    private $shopifyProductDao;
    private $failedGetCount;
    private $failedProductfetchingCount;

    public function __construct()
    {
        $this->setVariables();
        $this->setPaths();
        $this->getLogFilesList();
        $this->setModels();
        $this->process();
    }

    public function setVariables()
    {
        $this->debug = true;
        $this->storeName = 'lol-dev';
        $this->failedGetCount = 0;
        $this->failedProductfetchingCount = [];
    }

    public function setPaths()
    {
        $this->logDirPath = dirname(__FILE__) . '/../../log/';
        $this->csvFilePath = dirname(__FILE__) . '/../../mapping/';
    }

    public function setModels()
    {
        $this->shopifyProductDao = new shopifyProduct($this->storeName);
    }

    public function getLogFilesList()
    {

        $this->reportFileName = "mapping_log" . date('Y-m-d') . ".txt";
        $this->logFiles = array(
            'log' => array(
                "type" => "text",
                "filename" => $this->logDirPath . $this->reportFileName,
                "mode" => ""),
            'output' => array(
                "type" => "csv",
                "filename" => $this->csvFilePath . 'product-mapping.csv',
                "mode" => "",
                "columns" => array('PRODUCT_ID', 'SKU')),
            'lastExecutionFile' => array(
                "type" => "text",
                "filename" => $this->logDirPath . "status.txt",
                "mode" => ""),
        );

        $this->setWithLogFiles();

    }

    public function setWithLogFiles()
    {

        if ($this->logFiles) {
            foreach ($this->logFiles as $fileDetails) {
                $mode = !empty($fileDetails['mode']) ? $fileDetails['mode'] : "w";
                $fileName = $fileDetails['filename'];
                if ($fileDetails['type'] == "csv" && $mode != "r") {
                    $filePointer = fopen($fileName, $mode);
                    $columns = $fileDetails['columns'];
                    // fputs($filePointer, implode("|", array_map(array($this, 'encodeFunc'), $columns)) . "\r\n");
                    // $encodedColumns = explode("|", implode('|', array_map(array($this, 'encodeFunc'), $columns)));
                    fputcsv($filePointer, $columns);
                    fclose($filePointer);
                } else {
                    if (!file_exists($fileName)) {
                        $content = "";
                        touch($fileName);
                        //Make it writeable
                        chmod($fileName, 0777);
                    }
                }
            }
        }

    }

    public function process()
    {
        $status = $this->setExecutionTime();
        $lastExecutionFile = $this->logFiles['lastExecutionFile']['filename'];

        if ($status) {
            $writeHandle = fopen($lastExecutionFile, "w");
            $executionTime = date('Y-m-d H:i:s');
            fputcsv($writeHandle, array($executionTime, "Working"));

            $this->echoMe('Reading Shopify products');
            $this->logEntry('Reading Shopify products');
            $this->getAllShopifyProducts();
            $this->echoMe('Reading shopify products completed...');
            $this->logEntry('Reading shopify products completed...');

            fclose($writeHandle);
        }

        $writeHandle = fopen($lastExecutionFile, "w");
        $executionTime = date('Y-m-d H:i:s');
        fputcsv($writeHandle, array($executionTime, "Done"));
        fclose($writeHandle);
    }

    public function getAllShopifyProducts()
    {
        $limit = 250;

        $fetchedCount = 0;
        $totalProducts = 0;
        //$totalProducts = $this->getCount();

        $fp = fopen($this->logFiles['output']['filename'], "a");

        $pageNum = 1;
        do {

            try {
                $this->echoMe('Calling Shopify page no. ' . $pageNum);
                $this->failedProductfetchingCount[$pageNum] = 0;
                if (isset($products['link']) && !empty($products['link'])) {
                    $products = $this->shopifyProductDao->getShopifyproducts($limit, $products['link']);
                } else {
                    $products = $this->shopifyProductDao->getShopifyproducts($limit);
                }
            } catch (Exception $e) {

                $this->echoMe("Error form shopify when fetching the product details and error is -  " . $e->getMessage());
                $this->echoMe("Error form shopify when fetching the product details and error is -  " . $e->getMessage());
            }

            if (isset($products['body']) && !empty($products['body'])) {

                foreach ($products['body'] as $product) {

                    $productId = (isset($product['id']) && $product['id'] > 0) ? $product['id'] : false;

                    if (isset($product['variants']) && $productId) {

                        foreach ($product['variants'] as $variant) {

                            if (isset($variant['sku']) && !empty($variant['sku'])) {
                                $sku = $variant['sku'];
                                $row = array($productId, $sku);
                                // fputs($fp, implode("|", array_map(array($this, 'encodeFunc'), $row)) . "\r\n");
                                // $encodedRow = explode("|", implode('|', array_map(array($this, 'encodeFunc'), $row)));
                                fputcsv($fp, $row);
                            }

                        }

                    }

                }

                $productCount = count($products['body']);
                $fetchedCount += $productCount;
                $pageNum++;

            } else {

                $this->logEntry('Failed to get product from shopify for page');
                $this->logEntry(json_encode($products));
                $this->logEntry('Execution stoped.');
                exit();

            }

            if (empty($products['link'])) {
                break;
            }

        } while ($limit == $productCount);

    }

    public function encodeFunc($value)
    {
        //force wrap value in quotes and return
        $value = str_replace('"', '""', $value);
        return '"' . $value . '"';
    }

    /**
     * Set for all log Details
     * @param  integer $fileToLog  Log file path
     * @param  sting   $messgae    Log message
     * @return
     */
    public function logEntry($messgae)
    {
        // global $fileToLog;
        $log = new Logging();
        $log->lfile($this->logFiles['log']['filename']);
        $log->lwrite($messgae, true);
        $log->lclose();
    }

    public function getCount()
    {
        $this->failedGetCount++;
        if ($this->failedGetCount > 5) {
            $this->logEntry('Failed to get product count from shopify. Execution stoped');
            exit();
        }
        $count = $this->shopifyProductDao->getShopifyProductsCount();
        if (isset($count['count'])) {
            $count = $count['count'];
        } else {
            $count = $this->getCount();
        }
        return $count;
    }

    public function echoMe($str)
    {
        //  global $debug;
        if ($this->debug) {
            echo $str . PHP_EOL;
        }
    }

    public function setExecutionTime()
    {
        $executionTime = date('Y-m-d H:i:s');
        $index = 0;

        $lastExecutionFile = $this->logFiles['lastExecutionFile']['filename'];

        $readHandle = fopen($lastExecutionFile, "r");
        $fileData = fgetcsv($readHandle);
        $continue = true;
        $i = 0;
        if (!empty($fileData)) {
            while (!feof($readHandle)) {

                // echo ++$i.PHP_EOL;

                if (strtolower(trim($fileData[1])) == 'working') {

                    //logEntry('Working...');
                    $currentTime = strtotime(date('Y-m-d H:i:s'));
                    $previousTime = strtotime($fileData[0]);
                    $differenceInSec = $currentTime - $previousTime;
                    $diffInHours = ($differenceInSec) / 60;

                    if ($diffInHours <= 60) {
                        $this->logEntry('existing cron job working stopped execution...');
                        die('an existing cron job working');

                    } else {
                        //logEntry('Started processing again after working');
                        $continue = true;

                    }
                } else {

                    //logEntry('Started processing again ....');
                    $continue = true;

                }
                break;
            }

        } else {
            $continue = true;
            //logEntry('Started Processing from beginning');
        }

        return $continue;

    }

}

$productImport = new productImport();
