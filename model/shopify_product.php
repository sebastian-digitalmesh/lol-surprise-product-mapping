<?php

require_once dirname(__FILE__) . '/shopify_main.php';

class shopifyProduct
{

    private $shopifyObj;

    public function __construct($store)
    {

        $this->shopifyObj = new shopify($store);

    }

    /**
     * Getting shopify products based on page no. and limit in api call
     */
    public function getShopifyproducts($limit = null, $url = null)
    {

        $method = 'GET';
        //$path   = '/admin/products.json?limit='.$limit.'&fields=id,title,product_type,tags,variants';
        $path = '/admin/api/2020-01/products.json?limit=' . $limit;
        if ($url != null) {
            $response = $this->shopifyObj->shopifyClientObj->callPaging($method, $url, [], true);
        } else {
            $response = $this->shopifyObj->shopifyClientObj->callPaging($method, $path);
        }

        //$response = $this->shopifyObj->shopifyClientObj->call($method, $path);

        if ($response) {
            return $response;
        }
        return null;
    }

    public function getShopifyProductWithTitle($title = null)
    {

        $method = 'GET';
        $path = '/admin/products.json?title=' . $title;
        $params = '';

        $products = $this->shopifyObj->shopifyClientObj->call($method, $path);
        if ($products) {

            return $products;
        }
        return null;
    }

    public function getShopifyProductWithSku($sku = null)
    {

        $method = 'GET';
        $path = '/admin/products.json?sku=' . $sku;
        $params = '';

        $products = $this->shopifyObj->shopifyClientObj->call($method, $path);
        if ($products) {

            return $products;
        }
        return null;
    }

    public function getShopifyProductsCount()
    {

        $method = 'GET';
        $path = '/admin/products/count.json';
        $params = '';

        $count = $this->shopifyObj->shopifyClientObj->call($method, $path);
        if ($count) {

            return $count;
        }
        return null;
    }

    public function getShopifyproductById($id = null)
    {

        $method = 'GET';
        $path = '/admin/products.json?id=' . $id;
        $params = '';

        $products = $this->shopifyObj->shopifyClientObj->call($method, $path);
        if ($products) {

            return $products;
        }
        return null;
    }

    public function createShopifyproducts($request)
    {

        $method = 'POST';
        $path = '/admin/products.json';
        $params = $request;

        $response = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);

        if ($response) {

            return $response;
        }
        return null;

    }
    public function updateShopifyproducts($shopifyProductId, $request)
    {

        $method = 'PUT';
        $path = '/admin/products/' . $shopifyProductId . '.json';
        $params = $request;
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);
        //         echo '<pre>';
        //         print_r($response);
        return $response;

    }
    public function updateShopifyVariant($shopifyProductId, $variantID, $request)
    {

        $method = 'PUT';
        $path = '/admin/products/' . $shopifyProductId . '/variants/' . $variantID . '.json';
        $params = $request;
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);
        //         echo '<pre>';
        //         print_r($response);
        return $response;

    }

    public function importImage($shopifyProductId, $requestArray)
    {

        $method = 'POST';
        $path = '/admin/products/' . $shopifyProductId . '/images.json';
        $params = $requestArray;
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);
        //         echo '<pre>';
        //         print_r($response);
        return $response;

    }
    public function createProductMetaField($productId, $data)
    {

        $method = 'POST';
        $path = '/admin/products/' . $productId . '/metafields.json';
        $params = $data;
        $response = $this->shopifyObj->shopifyClientObj->call($method, $path, $params);
        //         echo '<pre>';
        //         print_r($response);
        return $response;

    }

}

//new shopifyProduct();
