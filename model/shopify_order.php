<?php 

require_once dirname(__FILE__).'/shopify_main.php';

class shopifyOrder{
	
	private $shopifyObj ;
	
	function __construct(){

		
		$this->shopifyObj = new shopify();
		
	}
	
	function importOrder($orderDetils){
	
		$method = 'POST';
		$path   = '/admin/orders.json';
		$params = $orderDetils;
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
	
		if($response){
			return $response;
		}
	 return null;
	}
	
	function updateOrder($orderDetils,$shopifyOrderId){
	
	
		
		$method = 'PUT';
		$path   = '/admin/orders/'.$shopifyOrderId.'.json';
		$params = $orderDetils;
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
	
		if($response){
			return $response;
		}
		return null;
	}
	
	
	function addTransaction($transactionDetails,$shopifyOrderId){
	
	
	
		$method = 'POST';
		$path   = '/admin/orders/'.$shopifyOrderId.'/transactions.json';
		$params = $transactionDetails;
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		if($response){
			return $response;
		}
		return null;
	}
	
	function getAllOrders($pageNo = NULL , $limit = NULL, $finaceStatus = NULL, $fields = [] ){
		
		$method = 'GET';
		$path   = '/admin/orders.json?limit='.$limit.'&page='.$pageNo.'&status=any&financial_status='.$finaceStatus;
		
		if(!empty($fields)){
		    $fields = implode(",", $fields);
		    $path .= '&fields=';
		    $path .= $fields;   
		}
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
		
		if($response){
			return $response;
		}
		return null;
	}
	
	function removeOrder($orderId){
		
		$method = 'DELETE';
		
		$path   = '/admin/orders/'.$orderId.'.json';
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
		return $response;
	}
	
	function cancelOrder($orderId){
	
		$method = 'POST';
		$path   = '/admin/orders/'.$orderId.'/cancel.json';
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
		return $response;
	}
	
	
	function getOrder($orderId){
	
		$method = 'GET';
		$path   = '/admin/orders/'.$orderId.'.json';
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
// 		echo '<pre>';
// 		print_r($response);
        return $response;
	}
	function getTransaction($orderId){
		
		$method = 'GET';
		$path   = '/admin/orders/'.$orderId.'/transactions.json';
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);
		// 		echo '<pre>';
		// 		print_r($response);
		return $response;
		
		
	}
	 function importRefund($shopifyOrderId,$RequesDetails){
		
		$method = 'POST';
		$path   = '/admin/orders/'.$shopifyOrderId.'/refunds.json';
		$params = $RequesDetails;
	
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path,$params);
		
		return $response;
		
	}

	function getOrdersWithDateFilter($startDate,$endDate)
	{
		$method = 'GET';
		$path   = '/admin/orders.json?status=any&created_at_min='.$startDate.'&created_at_max='.$endDate.' GMT -07:00';
		
		$response = $this->shopifyObj->shopifyClientObj->call($method, $path);

		if($response){
			return $response;
		}
		return null;
	}
}


//new shopifyOrder();