<?php

require_once dirname(__FILE__) . '/../lib/shopify.php';
require_once dirname(__FILE__) . '/../config/shopify_config.php';

class shopify
{

    protected $shopifyPass;
    protected $shopifyApiKey;
    protected $shopifySecreat;
    protected $shopifyDomin;
    public $shopifyClientObj;
    public $shopifyConfigObj;

    public function __construct($store)
    {
        $this->getShopifyConfig($store);
        $this->shopifyClientObj = new ShopifyClient($this->shopifyDomin,
            $this->shopifyPass,
            $this->shopifyApiKey,
            $this->shopifyPass);
    }
    /**
     * Getting config of shopify
     */
    public function getShopifyConfig($store)
    {

        switch ($store) {
            case "lol-dev":
                $this->shopifyConfigObj = new devConfig();
                break;
            case "lol-live":
                $this->shopifyConfigObj = new liveConfig();
                break;

        }
        $this->shopifyApiKey = $this->shopifyConfigObj->apiKey;
        $this->shopifyPass = $this->shopifyConfigObj->password;
        $this->shopifySecreat = $this->shopifyConfigObj->secret;
        $this->shopifyDomin = $this->shopifyConfigObj->shopDomain;

    }

}
