<?php
require_once dirname(__FILE__).'/../config/db_config.php';
class dbModel {
    private $conn;
    public function __construct(){
        $dbConfig = new dbConfig();
        
            $this->conn = new mysqli($dbConfig->host, $dbConfig->username, $dbConfig->password ,$dbConfig->db);
            if ($this->conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }
       
       
    }
    public function getSettings() {
        $sql = "SELECT * FROM settings";
        $result = $this->conn->query($sql);
        return $result;
    }
    public function getQueue() {
        $sql = "SELECT * FROM syncQueue";
        $result = $this->conn->query($sql);
        return $result;
    }
    public function updateQueueStatus($id, $data) {
        $status = $data["status"];
        $time = $data["start_time"];
        $sql = 'UPDATE syncQueue SET status = "'.$status.'", start_time = "'.$time.'" WHERE id='.$id;
        $result = $this->conn->query($sql);
        return $result;
    }

    public function updateQueue($id, $data) {
        $status = $data["status"];
        $end_time = $data["end_time"];
        $total_updates = $data["total_updates"];
        $total_fails = $data["total_fails"];
        $execution_time = $data["execution_time"];
        $sql = 'UPDATE syncQueue SET status = "'.$status.'", end_time = "'.$end_time.'", 
        total_updates="'.$total_updates.'", total_fails="'.$total_fails.'", 
        execution_time="'.$execution_time.'" WHERE id='.$id;
        $result = $this->conn->query($sql);
        return $result;
        
    }

    public function addQueue($data) {
        $status = $data["status"];
        $time = $data["start_time"];
        $sync_type = $data["sync_type"];
        $store = $data["store"];
        $sql='INSERT INTO syncQueue(store,sync_type,status,start_time,created_at) VALUES("'.$store.'","'.$sync_type.'","'.$status.'","'.$time.'","'.$time.'")';
        if($this->conn->query($sql) === true) {
            return $this->conn->insert_id;
        }
        return 0;
    }

}
